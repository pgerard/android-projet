package com.example.projetandroid;

import com.squareup.moshi.Json;

// Classe Museum ou on définit tout les attributs de la classe
public class Museum {


    @Json(name = "adresse")
    private String adresse;
    @Json(name = "cp")
    private String cp;
    @Json(name = "dept")
    private String dept;
    @Json(name = "ferme")
    private Boolean ferme;
    @Json(name = "fermeture_annuelle")
    private String fermeture_annuelle;
    @Json(name = "id")
    private String id;
    @Json(name = "nom")
    private String nom;
    @Json(name = "periode_ouverture")
    private String periode_ouverture;
    @Json(name = "region")
    private String region;
    @Json(name = "site_web")
    private String site_web;
    @Json(name = "ville")
    private String ville;

    // Constructeur de la classe
    public Museum(String id, String nom, String periode_ouverture, String adresse, String ville, boolean ferme, String fermeture_annuelle, String site_web, String cp, String region, String dept) {
        this.id = id;
        this.nom = nom;
        this.periode_ouverture = periode_ouverture;
        this.adresse = adresse;
        this.ville = ville;
        this.ferme = ferme;
        this.fermeture_annuelle = fermeture_annuelle;
        this.site_web = site_web;
        this.cp = cp;
        this.region = region;
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "{" +
                "adresse=\"" + adresse + '\"' +
                ", cp=\"" + cp + '\"' +
                ", dept=\"" + dept + '\"' +
                ", ferme=" + ferme +
                ", fermeture_annuelle=\"" + fermeture_annuelle + '\"' +
                ", id=\"" + id + '\"' +
                ", nom=\"" + nom + '\"' +
                ", periode_ouverture=\"" + periode_ouverture + '\"' +
                ", region=\"" + region + '\"' +
                ", site_web=\"" + site_web + '\"' +
                ", ville=\"" + ville + '\"' +
                '}';
    }

    // Getter et setter
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Boolean getFerme() {
        return ferme;
    }

    public void setFerme(Boolean ferme) {
        this.ferme = ferme;
    }


    public String getFermeture_annuelle() {
        return fermeture_annuelle;
    }

    public void setFermeture_annuelle(String fermeture_annuelle) {
        this.fermeture_annuelle = fermeture_annuelle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPeriode_ouverture() {
        return periode_ouverture;
    }

    public void setPeriode_ouverture(String periode_ouverture) {
        this.periode_ouverture = periode_ouverture;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSite_web() {
        return site_web;
    }

    public void setSite_web(String site_web) {
        this.site_web = site_web;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
