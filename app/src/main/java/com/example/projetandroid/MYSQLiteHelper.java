package com.example.projetandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MYSQLiteHelper extends SQLiteOpenHelper {

    private static final String TAG = "MYSQLiteHelper";

    private static final String TABLE_NAME = "musee"; //nom de la table SQL
    private static final String COL1 = "ID"; //première colonne de la table stockant l'ID
    private static final String COL2 = "fiche_musee"; //deuxième colonne de la table stockant l'ensemble de la fiche sous forme de string
    private static final String COL3 = "photos_musee"; // ** NON UTILISE ** // troisème colonne de la table stockant la liste des url des photos du musée
    private static final String COL4 = "name_musee"; //quatrième colonne de la table stockant l'ID

    private static MYSQLiteHelper mysqLiteHelper = null; //unique instance de la base de données


    public MYSQLiteHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    //crée la table lors de l'instanciation de la base de données
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable= "CREATE TABLE " + TABLE_NAME + " (" + COL1 + " TEXT, " +
                COL2 + " TEXT, " +
                COL3 + " TEXT, " +
                COL4 + " TEXT)";
        db.execSQL(createTable);
    }

    //** NON UTILISEE **//
    //méthode permettant de drop/supprimer la base de données et de la regénérée
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //méthode permettant d'ajouter un musée à la base de données SQL
    public boolean createMusee(String fiche) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase(); //donne la permission d'écrire
        ContentValues contentValues = new ContentValues();
        String ID = new JSONObject(fiche).getString("id"); //récupère le champ ID de l'objet fiche
        String name_musee = new JSONObject(fiche).getString("nom"); //récupère le champ nom de l'objet fiche
        //ajout des valeurs de chaque colonne de l'élément musée à ajouter à la variable ContentValues
        contentValues.put(COL1, ID);
        contentValues.put(COL2, fiche);
        contentValues.put(COL4, name_musee);

        Log.d(TAG, "createMusee: " + name_musee);

        //insert si musée n'existe pas déjà
        long result = -1;
        if (getCount(ID) == 0) {
            result = db.insert(TABLE_NAME, null, contentValues);
        }

        if(result == -1){
            return false;
        } else{
            return true;
        }

    }

    //vérifie si un musée existe déjà en BDD ou non
    public int getCount(String ID) {
        Cursor c = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE " + COL1 + " = ?";
        c = db.rawQuery(query, new String[] {ID});
        if (c.moveToFirst()) {
            return c.getInt(0);
        }
        return 0;
    }

    //return la fiche d'un musée en passant en paramètre le nom du musée
    public String getMuseeFiche(String musee_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String fiche = "";
        String selectQuery = "SELECT " + COL2 + " FROM " + TABLE_NAME + " WHERE " + COL4 + " = \"" + musee_name +"\"";
        Log.d(TAG, "getMuseeFiche: " + selectQuery);
        try {
            cursor = db.rawQuery( selectQuery, null);
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                fiche = cursor.getString(cursor.getColumnIndex(COL2));
            }
            Log.d(TAG, "getMuseeFiche: " + fiche);
            return fiche;
        }finally {
            cursor.close();
        }

    }

    //return la fiche du musée en passant en paramètre l'ID du musée
    public String getMuseeFicheById(String ID) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String fiche = "";
        String selectQuery = "SELECT " + COL2 + " FROM " + TABLE_NAME + " WHERE " + COL1 + " = \'" + ID +"\'";
        Log.d(TAG, "getMuseeFiche: " + selectQuery);
        try {
            cursor = db.rawQuery( selectQuery, null);
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                fiche = cursor.getString(cursor.getColumnIndex(COL2));
            }
            Log.d(TAG, "getMuseeFicheById: " + fiche);
            return fiche;
        }finally {
            cursor.close();
        }
    }

    //return l'ID d'un musée en passant en paramètre le nom du musée
    public String getMuseeIdByName(String name_musee) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String name = "";
        String selectQuery = "SELECT " + COL1 + " FROM " + TABLE_NAME + " WHERE " + COL4 + " = \"" + name_musee +"\"";
        Log.d(TAG, "getMuseeFiche: " + selectQuery);
        try {
            cursor = db.rawQuery( selectQuery, null);
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                name = cursor.getString(cursor.getColumnIndex(COL1));
            }
            Log.d(TAG, "getMuseeFicheById: " + name);
            return name;
        }finally {
            cursor.close();
        }
    }

    //return toutes les fiches du musées sotckées dans la base de données
    public List<String> getAllMusee() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        List<String> listMusee = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        try {
            cursor = db.rawQuery( selectQuery, null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String fiche = cursor.getString(cursor.getColumnIndex(COL2));
                    listMusee.add(fiche);
                    cursor.moveToNext();
                }
            }
            Log.d(TAG, "getAllMusee: "+listMusee);
            return listMusee;
        }finally {
            cursor.close();
        }
    }


    //récupère l'instance de la bdd si celle-ci est déjà instanciée sinon l'instancie
    public static MYSQLiteHelper getInstance(Context context) {
        if(mysqLiteHelper == null)
        {
            mysqLiteHelper = new MYSQLiteHelper(context.getApplicationContext());
        }
        return mysqLiteHelper;
    }
}