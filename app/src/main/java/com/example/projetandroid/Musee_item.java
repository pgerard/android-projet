package com.example.projetandroid;


// Modèle de la recylcerview pour la liste des musées scannés
public class Musee_item {

    private String museum_name;

    public Musee_item(String museum_name) {
        this.museum_name = museum_name;
    }

    public String getMuseum_name() {
        return museum_name;
    }

    public void setMuseum_name(String museum_name) {
        this.museum_name = museum_name;
    }

}
