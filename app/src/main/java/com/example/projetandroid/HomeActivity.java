package com.example.projetandroid;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.projetandroid.ReclycerView.MyMuseumAdapter;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

//
public class HomeActivity extends AppCompatActivity {

    //Permet de vérifier que l'utilisateur prends une seule photo
    private static final int PERMISSION_EXTERNAL_STORAGE = 1;

    private Button button_get_museum;
    private List<Musee_item> musee_items;
    private List<String> namemusee;
    private RecyclerView recyclerView;
    private MyMuseumAdapter myMuseumAdapter;
    private Context context;
    private Button button_info; // Permet d'avoir des informations sur l'application

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        button_get_museum= (Button) findViewById(R.id.button_QR_Code);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewMuseum);
        button_info= (Button) findViewById(R.id.button_help) ;
        musee_items = new ArrayList<>();
        namemusee = new ArrayList<>();
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        context = this;
        final MYSQLiteHelper myDatabaseHelper = MYSQLiteHelper.getInstance(context);

        // Permet de vérifier que l'utilisateur a bien donner la permission de stockage
      if (!(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
          requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_EXTERNAL_STORAGE);

      }
            //Permet de faire un appel à la base de donnée pour récuperer tout les musées déja scanné
            namemusee = myDatabaseHelper.getAllMusee();

            for (int i = 0; i < namemusee.size(); i++) {

                //Permet de récupérer tout les noms de musées déja scanné
                try {
                    String nom = new JSONObject(myDatabaseHelper.getAllMusee().get(i)).getString("nom");
                    musee_items.add(new Musee_item(nom));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            // Envoie cette list de String de musée à l'adapter
            myMuseumAdapter = new MyMuseumAdapter(musee_items);
            //Permet de faire la recycler view
            recyclerView.setAdapter(myMuseumAdapter);


            //Si l'utilisateur veux scanner un Qrcode, ouvre une nouvelle activité
            button_get_museum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), QRcode.class));
                }
            });

            button_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),Informations.class));
                }
            });


        }



    // Permet de demander la permission si elle n'a pas été donné par l'utilisateur
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permet d'afficher un message à l'utilisateur
                Toast.makeText(this, "Vous avez donné la permission", Toast.LENGTH_SHORT).show();
            } else {
                //Permet d'afficher un message à l'utilisateur
                Toast.makeText(this, "Vous n'avez pas donné la permission de stockage", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
