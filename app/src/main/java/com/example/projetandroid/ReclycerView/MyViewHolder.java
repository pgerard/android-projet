package com.example.projetandroid.ReclycerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.example.projetandroid.Photo;
import com.example.projetandroid.R;


// Classe utiliser pour la RecyclerView
public class MyViewHolder extends RecyclerView.ViewHolder {


    private ImageView photomusee;

    public MyViewHolder(View itemView) {
        super(itemView);
        photomusee= (ImageView)itemView.findViewById(R.id.photo);
    }

    public void setPhoto (Photo photo){

        photomusee = (ImageView)itemView.findViewById(R.id.photo);

        // Glider permet d'afficher l'image via l'url qu'on a récupérer en appelant l'APi
        Glide.with(photomusee.getContext()).load(photo.getUrl()).into(photomusee);
    }
}
