package com.example.projetandroid.ReclycerView;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projetandroid.AfficherMusee;
import com.example.projetandroid.Musee_item;
import com.example.projetandroid.R;

import java.util.List;


public class MyMuseumAdapter extends RecyclerView.Adapter<MyViewHolderMuseum>{

    private List<Musee_item> musee_items; // liste contenant l'ensemble des musées
    private Context context;

    public MyMuseumAdapter(List<Musee_item> musee_items) {
        this.musee_items = musee_items;
    }


    public MyViewHolderMuseum onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view= layoutInflater.inflate(R.layout.museum_item,parent,false);
        return new MyViewHolderMuseum(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolderMuseum holder, final int position) {
        holder.displayListMuseum(musee_items.get(position));
        // Permet de savoir sur quel item de la RecyclerView l'utilisateur a cliqué
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context= v.getContext();
                Intent intent = new Intent(context, AfficherMusee.class);
                intent.putExtra("NOM",musee_items.get(position).getMuseum_name());
                //Permet de récuperer le nom du musée sur lequel l'utilisateur à choisi
                context.startActivity(intent) ;
            }
        });


    }

    @Override
    public int getItemCount() {
        return musee_items.size();
    }
}
