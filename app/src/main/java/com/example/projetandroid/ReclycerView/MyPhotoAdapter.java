package com.example.projetandroid.ReclycerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projetandroid.Photo;
import com.example.projetandroid.R;

import java.util.List;

public class MyPhotoAdapter extends RecyclerView.Adapter<MyViewHolder> {

private List<Photo> photomusee;

    public MyPhotoAdapter(List<Photo> photomusee) {
        this.photomusee = photomusee;
    }

    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view= layoutInflater.inflate(R.layout.photo_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, int position) {
        holder.setPhoto(photomusee.get(position));

    }

    @Override
    public int getItemCount() {
        return photomusee.size();
    }
}
