package com.example.projetandroid.ReclycerView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.projetandroid.Musee_item;
import com.example.projetandroid.R;


//Classe étant utilisé pour les RecyclerView
public class MyViewHolderMuseum extends RecyclerView.ViewHolder {

    RelativeLayout relativeLayout;
    private TextView textView;


    public MyViewHolderMuseum(final View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.namemuseum);
        relativeLayout= itemView.findViewById(R.id.relativemuseum);


    }

    public void displayListMuseum(Musee_item musee_item) {
        textView = (TextView) itemView.findViewById(R.id.namemuseum);
        relativeLayout= itemView.findViewById(R.id.relativemuseum);


        textView.setText(musee_item.getMuseum_name());

    }
}
