package com.example.projetandroid;


//modèle de la recyclerview affichant la liste des photos d'un musée
public class Photo {

    private String url;

    public Photo(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
