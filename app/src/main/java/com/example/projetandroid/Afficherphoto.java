package com.example.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.projetandroid.ReclycerView.MyPhotoAdapter;
import java.util.ArrayList;
import java.util.List;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Classe qui va permettre d'afficher les photos liée à un musée
public class Afficherphoto extends AppCompatActivity {

    private String id; // id du musée
    private List<Photo> photo; // liste des photos du musée
    private RecyclerView recyclerView; // recycler view des photos du musée
    private MyPhotoAdapter myPhotoAdapter; //liaison entre la recylcer view et la liste des photos
    private FloatingActionButton floatingActionButton; //bouton

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficherphoto);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.homePage);
        photo = new ArrayList<>();
        context = this;

        //Permet de faire une recycler view sur les photos et donc d'économiser des ressources pour le téléphone
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);

        //Récupere l'id du musée pour ensuite afficher les photos liée a ce musée
        id = getIntent().getStringExtra("ID");


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();


        final ApiService apiService = new Retrofit.Builder()
                .baseUrl(ApiService.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService.class);

        //Appel à l'API
        apiService.listfiles(id).enqueue(new Callback<List>() {
            @Override
            public void onResponse(Call<List> call, Response<List> response) {

                // Permet de recuperer une liste de String
                //Chaque string correspond a une URL de photo
                for (int i = 0; i < response.body().size(); i++) {
                    photo.add(new Photo((String) response.body().get(i)));
                }

                // Envoie de cette liste d'url de Photo à l'adapter
                myPhotoAdapter = new MyPhotoAdapter(photo);
                //Cela va permettre de pouvoir affiher les photos
                recyclerView.setAdapter(myPhotoAdapter);


            }

            @Override
            public void onFailure(Call<List> call, Throwable t) {
                Toast.makeText(context, "Aucune photo disponible pour ce musée ou pas de connexion internet", Toast.LENGTH_SHORT).show();
            }
        });

        // Si l'utilisateur clique sur ce bouton, il revient sur la page d'acceuil
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);
            }
        });
    }

}