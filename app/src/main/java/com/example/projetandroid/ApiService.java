package com.example.projetandroid;


import android.media.Image;
import android.widget.ImageView;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

// Interface qui permet de declarer les requetes que nous allons faire à l'API
public interface ApiService {

    //url du webservice à appeler :
    public static final String ENDPOINT = "http://vps449928.ovh.net/api/";

    //get fiche musée
    @GET("musees/{id}")
    Call<Museum> getInfoMusee(@Path("id") String id);

    //get liste photos musée
    @GET("musees/{idMusee}/pictures")
    Call<List> listfiles(@Path("idMusee") String idMusee);

    //upload photos musée
    @Multipart
    @POST("musees/{idMusee}/pictures")
    Call<ResponseBody> uploadfile(@Part("description") RequestBody description, @Part MultipartBody.Part file, @Path("idMusee") String idMusee);

    // ** NON UTILISEE ** //
    //get photo files from musée
    @GET("musees/{idMusee}/pictures/{id}")
    Call<ImageView> getphoto(@Path("idMusee") String idmusee, @Path("id") String id);


    }
