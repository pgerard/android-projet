package com.example.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static android.support.v4.content.FileProvider.getUriForFile;


// Classe qui permet d'afficher un musée une fois qu'il a été scanné ou quand il a été enregistrer dans la base de données
public class AfficherMusee extends AppCompatActivity {

    // Déclartion de tous les attributs nécessaire pour afficher les informations du musée
    private static final String TAG = "";
    private String nameMusee;
    private TextView textView2;
    private TextView textView3;
    private TextView textView4;
    private TextView textView5;
    private TextView textView6;
    private TextView textView7;
    private TextView textView8;
    private TextView textView9;
    private TextView textView10;
    private TextView textView11;
    private Button buttonDisplayPhotos;
    private Button button_take_photo;
    private FloatingActionButton floatingActionButton;

    //Permet d'avoir accés à la base de donnée
    final MYSQLiteHelper myDatabaseHelper = MYSQLiteHelper.getInstance(this);
    //Permet d'être sur que l'utilisateur choisi bien une seul photo
    static final int REQUEST_TAKE_PHOTO = 1;
    private String currentPhotoPath;
    //Permet d'utiliser le context dans les fonctions de la classe
    private Context context;
    private String id2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //On récupere l'ID  qui provient du QRcode
        final String id = getIntent().getStringExtra("ID");
        //Permet d'obtenir le nom du musée si l'utilsateur veut voir un musée qu'il a déja scanné
        nameMusee = getIntent().getStringExtra("NOM");
        id2 = id;
        context = this;

        setContentView(R.layout.activity_afficher_musee);
        textView2 = (TextView) findViewById(R.id.nom);
        textView3 = (TextView) findViewById(R.id.periode_ouverture);
        textView4 = (TextView) findViewById(R.id.adresse);
        textView5 = (TextView) findViewById(R.id.ville);
        textView6 = (TextView) findViewById(R.id.ferme);
        textView7 = (TextView) findViewById(R.id.fermeture_annuelle);
        textView8 = (TextView) findViewById(R.id.site_web);
        textView9 = (TextView) findViewById(R.id.cp);
        textView10 = (TextView) findViewById(R.id.region);
        textView11 = (TextView) findViewById(R.id.dept);
        buttonDisplayPhotos = (Button) findViewById(R.id.button_display_photo);
        button_take_photo = (Button) findViewById(R.id.button_photo);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.homePage2);


        // Permet de logger les actions (données envoyées et recu)
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        //Permet de réaliser nos appels à l'API
        final ApiService apiService = new Retrofit.Builder()
                .baseUrl(ApiService.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService.class);


        //if museum exists and user click on museum element
        if (id == null) {
            displayFicheMusee(myDatabaseHelper.getMuseeFiche(nameMusee));

            //if user scaned a QRCode
        } else {
            //if museum scanned isn't in database yet
            if (myDatabaseHelper.getMuseeFicheById(id) == "") {
                apiService.getInfoMusee(id).enqueue(new Callback<Museum>() {
                    @Override
                    public void onResponse(Call<Museum> call, Response<Museum> response) {

                        //Enregistrement du musée dans la base de donnée
                        Museum p = response.body();
                        Log.d(TAG, "onResponse: " + response.body().getNom());
                        try {
                            myDatabaseHelper.createMusee(p.toString());
                            displayFicheMusee(myDatabaseHelper.getMuseeFicheById(id));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Museum> call, Throwable t) {
                        Toast.makeText(context, "Vous n'avez pas acces à internet", Toast.LENGTH_SHORT).show();
                    }
                });
                // if museum scanned is already in database
            } else {
                // On affiche toutes les données liées au musée
                displayFicheMusee(myDatabaseHelper.getMuseeFicheById(id));
            }
        }

        //Si l'utilisateur décide de voir les photos liée au musée, cela decleche un action
        buttonDisplayPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Afficherphoto.class);
                if (id != null) {
                    intent.putExtra("ID", id);
                } else {
                    intent.putExtra("ID", myDatabaseHelper.getMuseeIdByName(nameMusee));
                }
                startActivity(intent);


            }
        });


        // Si l'utilisateur décide de  prendre une photo d'un musée cela decleche un action
        button_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // On démarre la caméra
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //Création d'un fichier vide
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = getUriForFile(
                            context,
                            "com.example.android.fileprovider",
                            photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(intent, REQUEST_TAKE_PHOTO);
                }
            }
        });

        //Si l'utilisateur clique sur le floating button on revient sur la page principale
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);
            }
        });
    }

// Fonction qui permet de récuperer le nom du musee, le code postal.. et d'afficher ces informations à l'utilisateur
    public void displayFicheMusee(String fiche) {
        try {
            String nom = new JSONObject(fiche).getString("nom");
            // Vérification à chaque fois pour savoir si le champ est null ou non
            if (nom.compareTo("null") == 0) {
                // Si le champ est nul on affiche "non connu"
                textView2.setText("Non connu");
            } else textView2.setText(nom);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String periode_ouverture = new JSONObject(fiche).getString("periode_ouverture");
            if (periode_ouverture.compareTo("null") == 0) {
                textView3.setText("Non connu");
            } else textView3.setText(periode_ouverture);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String adresse = new JSONObject(fiche).getString("adresse");
            if (adresse.compareTo("null") == 0) {
                textView4.setText("Non connu");
            } else textView4.setText(adresse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String ville = new JSONObject(fiche).getString("ville");
            if (ville.compareTo("null") == 0) {
                textView5.setText("Non connu");
            } else textView5.setText(ville);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String ferme = new JSONObject(fiche).getString("ferme");

            if (ferme.compareTo("null") == 0) {
                textView6.setText("Oui");
            } else if (ferme.compareTo("null") == 0) {
                textView6.setText("Non connu");
            } else textView6.setText("Non");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String fermeture_annuelle = new JSONObject(fiche).getString("fermeture_annuelle");
            if (fermeture_annuelle.compareTo("null") == 0) {
                textView7.setText("Non connu");
            } else textView7.setText(fermeture_annuelle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String site_web = new JSONObject(fiche).getString("site_web");
            if (site_web.compareTo("null") == 0) {
                textView8.setText("Non connu");

            } else textView8.setText(site_web);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String cp = new JSONObject(fiche).getString("cp");
            if (cp.compareTo("null") == 0) {
                textView9.setText("Non connu");
            } else textView9.setText(cp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String region = new JSONObject(fiche).getString("region");
            if (region.compareTo("null") == 0) {
                textView10.setText("Non connu");
            }
            textView10.setText(region);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String dept = new JSONObject(fiche).getString("dept");
            if (dept.compareTo("null") == 0) {
                textView11.setText("Non connu");
            }
            textView11.setText(dept);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode != RESULT_CANCELED) {

            // On créer un fichier avec le AboslutePath qu'on vient recupérer
            File imageFile = new File(currentPhotoPath);

            //Création d'un fichier Bitmap
            Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            // On va effectuer une roation de 90° pour le bitmap car il ne s'affichait pas dans le bon sens
            Matrix matrix = new Matrix();
            matrix.preRotate(90);

            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
            OutputStream os;
            // On va compresser le fichier bitmap pour ensuite le retransformer en fichier
            // Cela permet de réduire la taille des fichiers envoyer et ainsi ne ne pas dépasser la limite autorisé
            try {
                os = new FileOutputStream(imageFile);
                // Niveau de compression à 50
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);
                os.flush();
                os.close();
            } catch (Exception e) {

            }

            if (id2 != null) {
                // Appel la fonction qui va permettre d'effectuer le post de la photo
                uploadImage(imageFile, id2);
            } else {
            // Appel la fonction qui va permettre d'effectuer le post de la photo
                uploadImage(imageFile, myDatabaseHelper.getMuseeIdByName(nameMusee));
            }
        }
    }


    // Fonction qui va permettre de creer un fichier avec un nom unique
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    //Fonction qui va permettre de faire une requete à l'API et ainsi de poster une photo sur un serveur distant

    private void uploadImage(File maPhoto, String idmusee) {

        // Permet de logger les actions (données envoyées et recu)
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        //Permet de réaliser nos appels à l'API
        ApiService apiService = new Retrofit.Builder()
                .baseUrl(ApiService.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService.class);

        RequestBody requestFile =
                RequestBody.create(

                        MediaType.parse("image/jpeg"),
                        maPhoto
                );
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", maPhoto.getName(), requestFile);


        String descriptionString = "uploading photo";
        RequestBody description =
                RequestBody.create(
                        MultipartBody.FORM, descriptionString);


        Call<ResponseBody> call = apiService.uploadfile(description, body, idmusee);


        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(AfficherMusee.this, "Photo envoyé avec succés", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AfficherMusee.this, "Verifiez votre connexion internet pour envoyer votre photo", Toast.LENGTH_SHORT).show();
            }
        });
    }


}



