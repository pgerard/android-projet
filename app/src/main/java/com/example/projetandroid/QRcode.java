package com.example.projetandroid;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


// Implements permet d'ajouter les fonctionnalités permettant de lire un QRcode
public class QRcode extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final String TAG = "Qrcode";
    private static final int REQUEST_CAMERA = 1;
    private Button scanButton;
    private ZXingScannerView scannerView;
    private boolean connected = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Permet de vérifier que l'utilisateur a bien donné la permission d'utiliser la caméra
        if (!(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        }
        scanButton= (Button) findViewById(R.id.buttonqr);
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);

    }

    @Override
    public void handleResult(Result result) {

        //Cette méthode permet de vérifier si l'utilisateur à acces à internet ou non pour pouvoir récupérer les données d'un musée
        // Nous savons que cette méthode est "deprecated" ce qui indique que ce n'est pas conseillé d'utiliser cette méthode pour les développeurs
        //Nous avons essayé avec la bibliothéque NetworkAgentInfo sans succées.
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            connected = true;
        }
        else
            connected = false;
        if(connected) {
            onBackPressed();
            Intent intent = new Intent(this, AfficherMusee.class);
            // Permet de recupérer l'Id du musée scanné et d'envoyer cette valeur à une nouvelle activité
            intent.putExtra("ID", result.getText());
            startActivity(intent);

        }
        else{
            Toast.makeText(this, "Vous n'avez pas de connexion internet", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    //fonction permettant de demander la permission d'utiliser la caméra
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission pour la caméra donné", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Vous n'avez pas donné la permssion d'utiliser la caméra", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
